import React, { useState } from 'react'

import Alert from '@/components/Alert'
import CSVButton from '@/components/CSVButton'
import Loader from '@/components/Loader'
import TableUsers from '@/components/Table'

import {
	addAttributes,
	addDuplicateId,
	checkDuplicates,
	getNormalizeRow,
	getYupErrors,
	requiredCells,
} from '@/utils/utils'
import { validationSchema } from '@/utils/yupSchema'

export const App = () => {
	const [loading, setLoading] = useState(false)
	const [alert, setAlert] = useState({ text: '', show: false })

	const [data, setData] = useState([])

	const hideLoader = () => setLoading(false)

	const showLoader = () => {
		setAlert({
			show: false,
			text: '',
		})
		setLoading(true)
	}

	const onFileLoaded = async data => {
		let newData = []
		let isValid = true

		for (let i = 1; i < data.length; i++) {
			let normalizeRow = getNormalizeRow(...data[i])
			let errors = {}

			try {
				await validationSchema.validate(normalizeRow, {
					abortEarly: false,
				})
			} catch (e) {
				errors = getYupErrors(e.inner)
			}

			if (requiredCells(errors)) {
				isValid = false
				break
			}

			normalizeRow = addAttributes(normalizeRow, i, errors)
			newData.push(normalizeRow)
		}

		if (isValid) {
			const duplicates = checkDuplicates(newData, ['phone', 'email'])
			newData = addDuplicateId(newData, duplicates)

			setData(newData)
		} else {
			setAlert({
				show: true,
				text: 'Incorrect data in file',
			})
			setData([])
		}
		hideLoader()
	}

	const onError = err => {
		setAlert({
			show: true,
			text: err,
		})
	}

	return (
		<div className='container'>
			<CSVButton
				onError={onError}
				showLoader={showLoader}
				onFileLoaded={onFileLoaded}
			/>
			<TableUsers rows={data} />

			{alert.show && <Alert text={alert.text} />}

			{loading && <Loader />}
		</div>
	)
}
