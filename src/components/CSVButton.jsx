import React from 'react'
import CSVReader from '@/tools/CSVReader'
import * as PropTypes from 'prop-types'

const readerOptions = {
	skipEmptyLines: 'greedy',
	transform: value => value.trim(),
}

const CSVButton = ({ onError, showLoader, onFileLoaded }) => (
	<div className='csv__btn'>
		<CSVReader
			label='Import users'
			accept='.csv'
			onFileLoad={showLoader}
			onFileLoaded={onFileLoaded}
			onError={onError}
			parserOptions={readerOptions}
		/>
	</div>
)

CSVButton.propTypes = {
	showLoader: PropTypes.func,
	onError: PropTypes.func,
	onFileLoaded: PropTypes.func,
}

export default CSVButton
