import React from 'react'
import * as PropTypes from 'prop-types'

const Alert = ({ text }) => {
	return <div className='alert'>{text}</div>
}

Alert.propTypes = {
	text: PropTypes.string,
}

export default Alert
