import React from 'react'

import '../styles/loader.scss'

const Loader = () => (
	<div className='loader__wrapper'>
		<div className='lds-roller'>
			<div />
			<div />
			<div />
			<div />
			<div />
			<div />
			<div />
			<div />
		</div>
	</div>
)

export default Loader
