import React from 'react'
import PropTypes from 'prop-types'

import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { useStyles } from '@/utils/utils'

const Rows = ({ rows }) => {
	const classes = useStyles()

	return (
		<>
			{rows.map(row => (
				<TableRow key={row.id}>
					<TableCell align='left'>{row.id}</TableCell>
					<TableCell
						align='left'
						className={!row.fullName.valid ? classes.cellError : null}
					>
						{row.fullName.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.phone.valid ? classes.cellError : null}
					>
						{row.phone.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.email.valid ? classes.cellError : null}
					>
						{row.email.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.age.valid ? classes.cellError : null}
					>
						{row.age.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.experience.valid ? classes.cellError : null}
					>
						{row.experience.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.yearlyIncome.valid ? classes.cellError : null}
					>
						{typeof row.yearlyIncome.value === 'number'
							? row.yearlyIncome.value.toFixed(2)
							: row.yearlyIncome.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.hasChildren.valid ? classes.cellError : null}
					>
						{row.hasChildren.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.licenseStates.valid ? classes.cellError : null}
					>
						{row.licenseStates.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.expirationDate.valid ? classes.cellError : null}
					>
						{row.expirationDate.value}
					</TableCell>
					<TableCell
						align='left'
						className={!row.licenseNumber.valid ? classes.cellError : null}
					>
						{row.licenseNumber.value}
					</TableCell>
					<TableCell align='left'>{row.duplicateWith.join(',')}</TableCell>
				</TableRow>
			))}
		</>
	)
}

Rows.propTypes = {
	rows: PropTypes.array,
}

export default Rows
