import React from 'react'
import * as PropTypes from 'prop-types'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TablePagination from '@material-ui/core/TablePagination'
import TableCell from '@material-ui/core/TableCell'

import Rows from '@/components/TableRow'
import { useStyles } from '@/utils/utils'

const TableUsers = ({ rows }) => {
	const classes = useStyles()

	const [page, setPage] = React.useState(0)
	const [rowsPerPage, setRowsPerPage] = React.useState(10)

	const handleChangePage = (_, newPage) => {
		setPage(newPage)
	}

	const handleChangeRowsPerPage = event => {
		setRowsPerPage(parseInt(event.target.value, 10))
		setPage(0)
	}

	const rowsChunk = rows.slice(
		page * rowsPerPage,
		page * rowsPerPage + rowsPerPage
	)

	return (
		<TableContainer>
			<Table>
				<TableHead>
					<TableRow>
						<TableCell className={classes.cellBold}>ID</TableCell>
						<TableCell>Full name</TableCell>
						<TableCell align='left'>Phone</TableCell>
						<TableCell align='left'>Email</TableCell>
						<TableCell align='left'>Age</TableCell>
						<TableCell align='left'>Experience</TableCell>
						<TableCell align='left'>Yearly Income</TableCell>
						<TableCell align='left'>Has children</TableCell>
						<TableCell align='left'>License states</TableCell>
						<TableCell align='left'>Expiration date</TableCell>
						<TableCell align='left'>License number</TableCell>
						<TableCell align='left'>Duplicate with</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					<Rows rows={rowsChunk} />
				</TableBody>
			</Table>
			<TablePagination
				rowsPerPageOptions={[5, 10, 25]}
				component='div'
				count={rows.length}
				rowsPerPage={rowsPerPage}
				page={page}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</TableContainer>
	)
}

TableUsers.propTypes = {
	rows: PropTypes.array,
}

export default TableUsers
