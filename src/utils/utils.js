import makeStyles from '@material-ui/core/styles/makeStyles'

const isInt = value => /^[+-]?[0-9]+$/.test(value)
const isFloat = value => /^[+-]?([0-9]*[.])?[0-9]+$/.test(value)
const hasHumber = value => /\d/.test(value)

const getLicenseStates = value => {
	return value
		.split('|')
		.map(val => val.slice(0, 2).toUpperCase())
		.join(',')
}

const getNormalizeRow = (
	fullName = '',
	phone = '',
	email = '',
	age = '',
	experience = '',
	yearlyIncome = '',
	hasChildren = '',
	licenseStates = '',
	expirationDate = '',
	licenseNumber = ''
) => {
	return {
		fullName,
		phone,
		email,
		age: isInt(age) ? +age : age,
		experience: isInt(experience) ? +experience : experience,
		yearlyIncome:
			isFloat(yearlyIncome) || isInt(yearlyIncome)
				? +yearlyIncome
				: yearlyIncome,
		hasChildren: hasChildren ? hasChildren : 'FALSE',
		licenseStates: hasHumber(licenseStates)
			? licenseStates
			: getLicenseStates(licenseStates),
		expirationDate,
		licenseNumber,
	}
}

const useStyles = makeStyles({
	cellBold: {
		fontWeight: 'bold',
	},
	cellError: {
		backgroundColor: '#F4CCCC',
	},
})

const requiredCells = errors => {
	return (
		errors.email === 'Required' ||
		errors.fullName === 'Required' ||
		errors.phone === 'Required'
	)
}

const getYupErrors = errors => {
	return errors
		.filter(i => !!i.message)
		.reduce(
			(curr, next) => ({
				...curr,
				[next.path]: next.errors[0],
			}),
			{}
		)
}

const normalizePhone = value => '+1' + value.slice(-10)

const addAttributes = (row, id, errors) => {
	const newRow = { id, duplicateWith: [] }

	for (const key of Object.keys(row)) {
		if (key === 'phone') {
			newRow[key] = {
				value: !errors[key] ? normalizePhone(row[key]) : row[key],
				valid: !errors[key],
			}
		} else {
			newRow[key] = {
				value: row[key],
				valid: !errors[key],
			}
		}
	}

	return newRow
}

const checkDuplicates = (arr, attributes) => {
	const duplicates = {}

	for (let i = 0; i < arr.length; i++) {
		for (const attribute of attributes) {
			if (!arr[i][attribute].valid) {
				continue
			}
			const key = arr[i][attribute].value.toLocaleString().toLowerCase()

			if (
				// eslint-disable-next-line no-prototype-builtins
				duplicates.hasOwnProperty(key)
			) {
				duplicates[key].indexes.push(i)
			} else {
				duplicates[key] = {
					indexes: [i],
					attribute,
				}
			}
		}
	}

	return duplicates
}

const addDuplicateId = (arr, duplicates) => {
	for (const key of Object.keys(duplicates)) {
		const { indexes, attribute } = duplicates[key]

		if (indexes.length > 1) {
			for (let i = 0; i < indexes.length; i++) {
				const idx = indexes[i]

				arr[idx].duplicateWith = [
					...new Set([
						...arr[idx].duplicateWith,
						arr[indexes[i === 0 ? 1 : 0]].id,
					]),
				]

				arr[idx][attribute].valid = false
			}
		}
	}

	return arr
}

export {
	getNormalizeRow,
	requiredCells,
	addAttributes,
	checkDuplicates,
	addDuplicateId,
	getYupErrors,
	useStyles,
}
