import * as Yup from 'yup'
import Moment from 'moment'

Yup.addMethod(Yup.string, 'format', function (formats, parseStrict, message) {
	return this.test({
		message,
		test(originalValue) {
			/*
			* Also we can delete MomentJs and validate by Regex
			* */
			let isValid = false

			for (let i = 0; i < formats.length; i++) {
				const value = Moment(originalValue, formats[i], parseStrict)

				isValid = value.isValid()

				if (isValid) {
					break
				}
			}

			return isValid
		},
	})
})

Yup.addMethod(Yup.string, 'min', function (date, message) {
	return this.test({
		message,
		test(originalValue) {
			return new Date(originalValue).getTime() > date.getTime()
		},
	})
})

const validationSchema = Yup.object({
	fullName: Yup.string()
		.matches(/^[a-zA-Z\s]+$/, 'Invalid full name')
		.required('Required'),
	phone: Yup.string()
		.matches(/^(\+1|1)?\d{10}$/, 'Invalid phone number')
		.required('Required'),
	email: Yup.string().email('Invalid email format').required('Required'),
	age: Yup.number().moreThan(20, 'Age < 21').required('Required'),
	experience: Yup.number()
		.moreThan(-1, 'experience < 0')
		.lessThan(Yup.ref('age'), 'Experience > Age')
		.required('Required'),
	yearlyIncome: Yup.number()
		.moreThan(-1, 'yearlyIncome < 0')
		.lessThan(1000000, 'yearlyIncome > 1 000 000')
		.required('Required'),
	hasChildren: Yup.string()
		.matches(/^(TRUE|FALSE)$/, 'Must be true or false')
		.required('Required'),
	licenseStates: Yup.string()
		.matches(/^[A-Z]{2}(,[A-Z]{2})*$/, 'Invalid license states')
		.required('Required'),
	expirationDate: Yup.string()
		.min(new Date(), 'Must be greater than today')
		.format(['YYYY-MM-DD', 'MM/DD/YYYY'], true, 'Invalid date format')
		.required('Required'),
	licenseNumber: Yup.string()
		.matches(/^[A-Za-z0-9]{6}$/, 'Must be character or number and length = 6')
		.required('Required'),
})

export { validationSchema }
