const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const OptimizeCssAssetWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')

const isProd = process.env.NODE_ENV === 'production'
const isDev = !isProd

const fileName = (ext) =>
	isDev ? `bundle.[name].${ext}` : `bundle.[contenthash].${ext}`

const optimization = () => {
	const config = {
		splitChunks: {
			chunks: 'all',
		},
	}

	if (isProd) {
		config.minimizer = [
			new OptimizeCssAssetWebpackPlugin(),
			new TerserWebpackPlugin(),
		]
	}

	return config
}

const cssLoaders = (extra) => {
	const loaders = [
		{
			loader: MiniCssExtractPlugin.loader,
		},
		'css-loader',
	]

	if (extra) {
		loaders.push(extra)
	}

	return loaders
}

const jsLoaders = () => {
	const loaders = ['babel-loader']

	if (isDev) {
		loaders.push('eslint-loader')
	}

	return loaders
}

const plugins = () => {
	const base = [
		new CleanWebpackPlugin(),
		new HTMLWebpackPlugin({
			template: path.resolve(__dirname, 'public/index.html'),
			minify: {
				removeComments: isProd,
				collapseWhitespace: isProd,
			},
		}),
		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, 'public/favicon.ico'),
					to: path.resolve(__dirname, 'build'),
				},
			],
		}),
		new MiniCssExtractPlugin({
			filename: fileName('css'),
		}),
	]

	return base
}

module.exports = {
	target: isDev ? 'web' : 'browserslist',
	context: path.resolve(__dirname, 'src'),
	entry: ['@babel/polyfill', './index.jsx'],
	output: {
		filename: fileName('js'),
		path: path.resolve(__dirname, 'build'),
	},
	resolve: {
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.scss'],
		alias: {
			'@': path.resolve(__dirname, 'src'),
		},
	},
	optimization: optimization(),
	devtool: isDev ? 'source-map' : false,
	devServer: {
		port: 4200,
		hot: isDev,
	},
	plugins: plugins(),
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/i,
				use: cssLoaders('sass-loader'),
			},
			{
				test: /\.(js|jsx|ts|tsx)$/,
				exclude: /node_modules/,
				use: jsLoaders(),
			},
		],
	},
}

// TODO 1. Jest support
// TODO 2. TS support / config
